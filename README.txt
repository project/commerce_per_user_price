CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
This module provides per product per user price for Drupal Commerce.
If you need one pricelist per user! That mean:
* If you have 500 customers that have different price per product on your ERP.
So one pricelist per user.
* And you have 1000 products.
* So, the list would be 500x1000=500000

REQUIREMENTS
------------
This module requires the following modules:
* Drupal Commerce(https://www.drupal.org/project/commerce)
* Commerce line item

INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
* Enable the Custom module on the Administer -> Modules page 
    (Under the "Commerce (contrib)" category).

CONFIGURATION
-------------
* Configure user permissions in Administration » People » Permissions:
- Commerce Per User Price

     Users that have permission "Commerce Per User Price" will see
     the changed price of each product.
* Insert new record in db for each product Administration » Store »
   Configuration » Custom price » Add Custom price.
* Here is the scenario of price table
id	product_id	user_id		price
1		1			1		4.2
2		2			1		2.3
3		1			2		4.9
4		2			2		3.6
5		1			3		1.2
6		2			3		2.2
